from django.urls import path, include
from . import views

app_name = "demo"

urlpatterns = [
    path('users', views.index, name='index'),
    path('users/create', views.create, name='create'),
    path('users/edit/<int:id>', views.edit, name='edit'),
    path('users/delete/<int:id>', views.delete, name='delete'),
]
