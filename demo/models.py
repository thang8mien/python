from django.db import models
import datetime


class Customers(models.Model):
    FirstName = models.CharField(max_length=50)
    LastName = models.CharField(max_length=50)
    FullName = models.CharField(max_length=100, blank=True)
    Password = models.CharField(max_length=50)
    Birthday = models.DateField()
    Hobbies = models.CharField(max_length=250)
    Description = models.TextField(blank=True)

    def save(self, *args, **kwargs):
        self.FullName = self.FirstName + ' ' + self.LastName
        if self.Birthday == '':
            self.Birthday = datetime.datetime.now()
        super().save(*args, **kwargs)


class Groups(models.Model):
    choice_permissions = (
        (0, 'N/A'),
        (1, 'ADD'),
        (2, 'EDIT'),
        (3, 'DELETE')
    )
    Name = models.CharField(max_length=100)
    Permissions = models.IntegerField(choices=choice_permissions, default=0)
    Description = models.CharField(max_length=250)


class Roles(models.Model):
    choice_permissions = (
        (0, 'N/A'),
        (1, 'ADMIN'),
        (2, 'PUBLISHER'),
        (3, 'APPROVER'),
        (4, 'MODERATOR'),
        (5, 'EDITOR'),
        (6, 'CREATOR'),
    )
    Name = models.CharField(max_length=100)
    Permissions = models.IntegerField(choices=choice_permissions, default=0)
    Description = models.CharField(max_length=250)


class Customer_Group(models.Model):
    Customer_ID = models.ForeignKey(Customers, on_delete=models.CASCADE)
    Group_ID = models.ForeignKey(Groups, on_delete=models.CASCADE)


class Customer_Role(models.Model):
    Customer_ID = models.ForeignKey(Customers, on_delete=models.CASCADE)
    Role_ID = models.ForeignKey(Roles, on_delete=models.CASCADE)
