from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from datetime import datetime

from .models import Customers, Groups, Roles

from pdb import set_trace as byebug


def index(request):
    rs = Customers.objects.all()
    context = {
        'data': rs,
    }
    return render(request, 'users/index.html', context)


def create(request):
    if request.method == "POST":
        rs = Customers(FirstName=request.POST['FirstName'],
                       LastName=request.POST['LastName'],
                       Password=request.POST['Password'],
                       Birthday=request.POST['Birthday'],
                       Hobbies=request.POST['Hobbies'],
                       Description=request.POST['Description'])

        rs.save()

        return HttpResponseRedirect(reverse('demo:edit', args=(rs.id,)))
    else:
        return render(request, 'users/form.html')


def edit(request, id):
    if request.method == "POST":
        rs = Customers.objects.get(pk=id)
        rs.FirstName = request.POST['FirstName']
        rs.LastName = request.POST['LastName']
        rs.Password = request.POST['Password']
        rs.Birthday = request.POST['Birthday']
        rs.Hobbies = request.POST['Hobbies']
        rs.Description = request.POST['Description']

        rs.save()

        return HttpResponseRedirect(reverse('demo:edit', args=(rs.id,)))
    else:
        rs = Customers.objects.get(pk=id)
        rs.Birthday = rs.Birthday.strftime("%Y-%m-%d")
        context = {
            'data': rs
        }
        return render(request, 'users/form.html', context)


def delete(request, id):
    rs = Customers.objects.get(pk=id)
    rs.delete()
    return HttpResponseRedirect(reverse('demo:index'))
